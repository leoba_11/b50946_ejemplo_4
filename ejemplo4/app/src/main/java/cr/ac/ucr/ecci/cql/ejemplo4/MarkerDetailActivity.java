package cr.ac.ucr.ecci.cql.ejemplo4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class MarkerDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marker_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Extraer lat. y lng.
        Intent intent = getIntent();
        String latlng = String.format(
                getString(R.string.marker_detail_latlng),
                intent.getDoubleExtra(MapsActivity.EXTRA_LATITUDE, 0),
                intent.getDoubleExtra(MapsActivity.EXTRA_LONGITUDE, 0));

        // Poblar
        TextView coordenadas = findViewById(R.id.tv_latlng);
        coordenadas.setText(latlng);
    }

    @Override
    public boolean onSupportNavigateUp() { onBackPressed();
        onBackPressed();
        return super.onSupportNavigateUp();
    }

}
